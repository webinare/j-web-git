# J-WEB-GIT - Übungen

# Repository konfigurieren

1. Ändern Sie ihren Namen als Autor für dieses Repository
2. Legen sie eine neue Datei an. In welchem Zustand befindet sich die Datei zu nächst?
3. Committen Sie die Datei.
4. Prüfen Sie den Namen des Autors in der Historie.

# Branching

1. Legen Sie einen neuen Branch mit dem Namen feature/{Ihr Name} an.
2. Fügen Sie eine neue Datei im angelegten Branch hinzu.
3. Committen Sie ihre Änderungen.
4. Wechseln Sie zurück in den ursprünglichen Branch. 
5. Prüfen Sie, dass die Datei nicht mehr vorhanden ist.

# Ignorieren von Dateien

1. Stellen Sie das Tracking von allen Dateien mit der Endung ".egal" aus.
2. Legen Sie eine Datei mit der Endung ".egal" an
3. Prüfen Sie den aktuellen Status ihres Working-Directories

# Merging

1. Mergen Sie den angelegten Branch in den Master
2. Legen sie einen neuen Branch an und ändern eine bereits vorhandene Datei (test.txt).
3. Commiten Sie die Änderungen
4. Wechseln Sie in den ursprünglichen Branch und ändern die selbe Datei auch hier (Am besten in der selben Zeile wie zuvor).
5. Comitten Sie die Änderungen.
6. Mergen Sie den neuen Branch in ihren aktuellen.
7. Es sollte nun einen Merge-Konflikt geben.
8. Lösen Sie den Konflikt und beenden den Merge.
9. Schauen Sie sich mit gitk alle Branches an.

# Rebase

1. Legen Sie einen neuen Branch an und verändern Sie eine vorliegende Datei
2. Committen Sie die Änderungen.
3. Wechseln Sie zurück in den master Branch.
4. Fügen Sie eine neue Datei ein und Committen die Änderungen.
5. Wechseln Sie zurück in ihren Branch und führen ein Rebase gegenüber dem master Branch durch.

# Rebase Interaktiv

1. Legen Sie einen neuen Branch an.
2. Ändern Sie mehrfach eine Datei oder legen neue Dateien an. Committen sie zwischenzeitlich immer wieder ihre Änderungen.
3. Prüfen Sie, dass in der Historie mehrere Commits hinterlegt sind.
4. Führen Sie nun alle Commits auf dem neuen Branch zusammen und vergeben eine neue Commit-Message

 # Tagging
 
1. Taggen Sie den aktuellen Stand ihres Master-Branches
2. Wechseln Sie zum aktuellen Tag (git checkout {Tag-Name})
3. Fügen Sie eine neue Datei hinzu und commiten Sie diese.
4. Was können Sie aus der UI gitk entnehmen?
